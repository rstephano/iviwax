package br.com.viwax.iviwax.controller;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import br.com.viwax.iviwax.model.Cliente;
import br.com.viwax.iviwax.model.Fornecedor;
import br.com.viwax.iviwax.model.Servico;
import br.com.viwax.iviwax.model.Usuario;
import br.com.viwax.iviwax.repositories.ClienteRepository;
import br.com.viwax.iviwax.repositories.FornecedorRepository;
import br.com.viwax.iviwax.repositories.ServicoRepository;
import br.com.viwax.iviwax.repositories.UsuarioRepository;

@ManagedBean
@ApplicationScoped
public class AppBean {
	private List<Usuario> usuarios;
	private List<Cliente> clientes;
	private List<Fornecedor> fornecedores;
	private List<Servico> servicos;

	private EntityManager getEntityManager() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		EntityManager manager = (EntityManager) request
				.getAttribute("EntityManager");
		return manager;
	}

	public List<Usuario> getUsuarios() {
		if (this.usuarios == null) {
			EntityManager manager = this.getEntityManager();
			UsuarioRepository repository = new UsuarioRepository(manager);
			this.usuarios = repository.buscaTodos();
		}
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<Cliente> getClientes() {
		if (this.clientes == null) {
			EntityManager manager = this.getEntityManager();
			ClienteRepository repository = new ClienteRepository(manager);
			this.clientes = repository.buscaTodos();
		}
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<Fornecedor> getFornecedores() {
		if (this.fornecedores == null) {
			EntityManager manager = this.getEntityManager();
			FornecedorRepository repository = new FornecedorRepository(manager);
			this.fornecedores = repository.buscaTodos();
		}
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

	public List<Servico> getServicos() {
		if (this.servicos == null) {
			EntityManager manager = this.getEntityManager();
			ServicoRepository repository = new ServicoRepository(manager);
			this.servicos = repository.buscaTodos();
		}
		return servicos;
	}

	public void setServicos(List<Servico> servicos) {
		this.servicos = servicos;
	}

}
