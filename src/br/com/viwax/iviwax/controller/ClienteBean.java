package br.com.viwax.iviwax.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

import br.com.viwax.iviwax.model.Cliente;
import br.com.viwax.iviwax.model.Contato;
import br.com.viwax.iviwax.model.Endereco;
import br.com.viwax.iviwax.repositories.ClienteRepository;

@ViewScoped
@ManagedBean
public class ClienteBean {
	private Cliente cliente = new Cliente();
	private Cliente clienteAnalisado = new Cliente();
	private int indiceClienteAnalisado;
	private Endereco enderecoClienteAnalisado;
	private Contato contatoClienteAnalisado;
	@ManagedProperty("#{appBean}")
	private AppBean appBean;
	private int activeTab = 0;

	private EntityManager getEntityManager() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		EntityManager manager = (EntityManager) request
				.getAttribute("EntityManager");
		return manager;
	}

	public void adicionaCliente(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		ClienteRepository repository = new ClienteRepository(manager);
		repository.adiciona(cliente);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Cadastro de  " + cliente.getNome()
						+ " realizado com sucesso"));
		this.cliente = new Cliente();
		this.appBean.setClientes(null);
	}

	public void adicionaEndereco(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		ClienteRepository repository = new ClienteRepository(manager);
		this.activeTab = 0;
		this.clienteAnalisado = repository
				.buscar(this.clienteAnalisado.getId());
		this.clienteAnalisado.getEnderecos().add(this.enderecoClienteAnalisado);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Endere�o adicionado"));
	}

	public void adicionaContato(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		ClienteRepository repository = new ClienteRepository(manager);
		this.activeTab = 1;
		this.clienteAnalisado = repository
				.buscar(this.clienteAnalisado.getId());
		this.clienteAnalisado.getContatos().add(this.contatoClienteAnalisado);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Contato adicionado"));
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cliente getClienteAnalisado() {
		return clienteAnalisado;
	}

	public void setClienteAnalisado(Cliente clienteAnalisado) {
		EntityManager manager = this.getEntityManager();
		ClienteRepository repository = new ClienteRepository(manager);
		this.clienteAnalisado = repository.buscar(clienteAnalisado.getId());
		this.clienteAnalisado.getEnderecos();
		this.enderecoClienteAnalisado = new Endereco();
	}

	public void zeraEnderecoClienteAnalisado(ActionEvent actionEvent) {
		this.enderecoClienteAnalisado = new Endereco();
	}

	public void zeraContatoClienteAnalisado(ActionEvent actionEvent) {
		this.contatoClienteAnalisado = new Contato();
	}

	public int getIndiceClienteAnalisado() {
		return indiceClienteAnalisado;
	}

	public void setIndiceClienteAnalisado(int indiceClienteAnalisado) {
		this.indiceClienteAnalisado = indiceClienteAnalisado;
	}

	public AppBean getAppBean() {
		return appBean;
	}

	public void setAppBean(AppBean appBean) {
		this.appBean = appBean;
	}

	public void onRowSelect(SelectEvent event) {

	}

	public Endereco getEnderecoClienteAnalisado() {
		return enderecoClienteAnalisado;
	}

	public void setEnderecoClienteAnalisado(Endereco enderecoClienteAnalisado) {
		this.enderecoClienteAnalisado = enderecoClienteAnalisado;
	}

	public Contato getContatoClienteAnalisado() {
		return contatoClienteAnalisado;
	}

	public void setContatoClienteAnalisado(Contato contatoClienteAnalisado) {
		this.contatoClienteAnalisado = contatoClienteAnalisado;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public void onTabChange(TabChangeEvent event) {
		if (this.activeTab == 0) {
			this.activeTab = 1;
		} else {
			this.activeTab = 0;
		}
	}
}
