package br.com.viwax.iviwax.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import br.com.viwax.iviwax.model.Contato;
import br.com.viwax.iviwax.model.Endereco;
import br.com.viwax.iviwax.model.Fornecedor;
import br.com.viwax.iviwax.model.Servico;
import br.com.viwax.iviwax.repositories.FornecedorRepository;
import br.com.viwax.iviwax.repositories.ServicoRepository;

@ViewScoped
@ManagedBean
public class FornecedorBean {
	private Fornecedor fornecedor = new Fornecedor();
	private Fornecedor fornecedorAnalisado = new Fornecedor();
	private int indiceFornecedorAnalisado;
	private Endereco enderecoFornecedorAnalisado;
	private Contato contatoFornecedorAnalisado;
	private Servico servicoFornecedorAnalisado = new Servico();
	private Servico novoServico = new Servico();
	@ManagedProperty("#{appBean}")
	private AppBean appBean;
	private List<Servico> servicosNaoSelecionados = new ArrayList<Servico>();
	private int activeTab = 0;

	private EntityManager getEntityManager() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		EntityManager manager = (EntityManager) request
				.getAttribute("EntityManager");
		return manager;
	}

	public void adicionaFornecedor(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		FornecedorRepository repository = new FornecedorRepository(manager);
		repository.adiciona(fornecedor);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Cadastro de  " + fornecedor.getNomeFantasia()
						+ " realizado com sucesso"));
		this.fornecedor = new Fornecedor();
		this.appBean.setFornecedores(null);
	}

	public void adicionaEndereco(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		FornecedorRepository repository = new FornecedorRepository(manager);
		this.activeTab = 0;
		this.fornecedorAnalisado = repository.buscar(this.fornecedorAnalisado
				.getId());
		this.fornecedorAnalisado.getEnderecos().add(
				this.enderecoFornecedorAnalisado);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Endere�o adicionado"));
	}

	public void adicionaContato(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		FornecedorRepository repository = new FornecedorRepository(manager);
		this.activeTab = 1;
		this.fornecedorAnalisado = repository.buscar(this.fornecedorAnalisado
				.getId());
		this.fornecedorAnalisado.getContatos().add(
				this.contatoFornecedorAnalisado);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Contato adicionado"));
	}

	public void adicionaServico(ActionEvent actionEvent) {
		this.activeTab = 2;
		if (this.servicoFornecedorAnalisado.getId() == null) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Imposs�vel incluir, escolha uma op��o v�lida",
							"N�o � poss�vel incluir este servi�o"));
		} else {
			EntityManager manager = this.getEntityManager();
			FornecedorRepository fRepository = new FornecedorRepository(manager);
			ServicoRepository sRepository = new ServicoRepository(manager);
			this.fornecedorAnalisado = fRepository
					.buscar(this.fornecedorAnalisado.getId());
			this.servicoFornecedorAnalisado = sRepository
					.buscar(this.servicoFornecedorAnalisado.getId());
			this.fornecedorAnalisado.getServicos().add(
					this.servicoFornecedorAnalisado);
			this.fornecedorAnalisado.getServicos();
			this.servicoFornecedorAnalisado = new Servico();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Servico adicionado"));
		}
	}

	public void adicionaNovoServico(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		ServicoRepository sRepository = new ServicoRepository(manager);
		this.activeTab = 2;
		this.novoServico.setId(null);
		sRepository.adiciona(this.novoServico);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Novo servi�o adicionado:"
						+ this.novoServico.getNome()));
		this.novoServico = new Servico();
		appBean.setServicos(null);
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Fornecedor getFornecedorAnalisado() {
		return fornecedorAnalisado;
	}

	public void setFornecedorAnalisado(Fornecedor fornecedorAnalisado) {
		EntityManager manager = this.getEntityManager();
		FornecedorRepository repository = new FornecedorRepository(manager);
		this.fornecedorAnalisado = repository.buscar(fornecedorAnalisado
				.getId());
		this.fornecedorAnalisado.getEnderecos();
		this.fornecedorAnalisado.getServicos();
		this.servicoFornecedorAnalisado = new Servico();
		this.enderecoFornecedorAnalisado = new Endereco();
	}

	public int getIndiceFornecedorAnalisado() {
		return indiceFornecedorAnalisado;
	}

	public void setIndiceFornecedorAnalisado(int indiceFornecedorAnalisado) {
		this.indiceFornecedorAnalisado = indiceFornecedorAnalisado;
	}

	public AppBean getAppBean() {
		return appBean;
	}

	public void setAppBean(AppBean appBean) {
		this.appBean = appBean;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public Contato getContatoFornecedorAnalisado() {
		return contatoFornecedorAnalisado;
	}

	public void setContatoFornecedorAnalisado(Contato contatoFornecedorAnalisado) {
		this.contatoFornecedorAnalisado = contatoFornecedorAnalisado;
	}

	public void zeraEnderecoFornecedorAnalisado(ActionEvent actionEvent) {
		this.enderecoFornecedorAnalisado = new Endereco();
	}

	public void zeraContatoFornecedorAnalisado(ActionEvent actionEvent) {
		this.contatoFornecedorAnalisado = new Contato();
	}

	public void zeraServicoFornecedorAnalisado(ActionEvent actionEvent) {
		this.servicoFornecedorAnalisado = new Servico();
	}

	public Endereco getEnderecoFornecedorAnalisado() {
		return enderecoFornecedorAnalisado;
	}

	public void setEnderecoFornecedorAnalisado(
			Endereco enderecoFornecedorAnalisado) {
		this.enderecoFornecedorAnalisado = enderecoFornecedorAnalisado;
	}

	public void atualizaServicos(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		FornecedorRepository repository = new FornecedorRepository(manager);
		List<Servico> servicos = fornecedorAnalisado.getServicos();
		this.activeTab = 3;
		this.fornecedorAnalisado = repository.buscar(this.fornecedorAnalisado
				.getId());
		this.fornecedorAnalisado.setServicos(servicos);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Servi�os atualizados"));
	}

	public Servico getServicoFornecedorAnalisado() {
		return servicoFornecedorAnalisado;
	}

	public void setServicoFornecedorAnalisado(Servico servicoFornecedorAnalisado) {
		this.servicoFornecedorAnalisado = servicoFornecedorAnalisado;
	}

	public List<Servico> getServicosNaoSelecionados() {
		this.servicosNaoSelecionados = new ArrayList<Servico>(
				appBean.getServicos());
		if (this.fornecedorAnalisado.getId() != null) {
			for (int i = 0; i < this.fornecedorAnalisado.getServicos().size(); i++) {
				this.servicosNaoSelecionados.remove(this.fornecedorAnalisado
						.getServicos().get(i));
			}
		}
		return this.servicosNaoSelecionados;
	}

	public void setServicosNaoSelecionados(List<Servico> servicosNaoSelecionados) {
		this.servicosNaoSelecionados = servicosNaoSelecionados;
	}

	public Servico getNovoServico() {
		return novoServico;
	}

	public void setNovoServico(Servico novoServico) {
		this.novoServico = novoServico;
	}

}
