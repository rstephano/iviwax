package br.com.viwax.iviwax.controller;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.viwax.iviwax.model.Usuario;
import br.com.viwax.iviwax.repositories.UsuarioRepository;

@ManagedBean
@SessionScoped
public class UsuarioBean {
	boolean loggedIn = false;
	@ManagedProperty("#{appBean}")
	private AppBean appBean;
	private Usuario usuarioLogado = new Usuario();
	private Usuario usuario = new Usuario();
	private Usuario usuarioAnalisado = new Usuario();

	private EntityManager getEntityManager() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		EntityManager manager = (EntityManager) request
				.getAttribute("EntityManager");
		return manager;
	}

	public void adicionaUsuario(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		repository.adiciona(usuario);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Cadastro de  " + usuario.getNome()
						+ " realizado com sucesso"));
		this.usuario = new Usuario();
		this.appBean.setUsuarios(null);
	}

	public String buscarUsuario(Long id) {
		EntityManager manager = this.getEntityManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		this.usuarioAnalisado = repository.buscar(id);
		return "/usuario/ver-usuario";
	}

	public void removeUsuario(ActionEvent actionEvent) {
		EntityManager manager = this.getEntityManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		repository.remover(usuarioAnalisado.getId());
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Cadastro de " + usuarioAnalisado.getNome()
						+ " removido com sucesso"));
		this.usuario = new Usuario();
		this.appBean.setUsuarios(null);
	}

	public void atualizaUsuarioAnalisado(Long id) {
		EntityManager manager = this.getEntityManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		repository.atualizar(usuarioAnalisado, id);
		this.usuarioAnalisado = new Usuario();
		this.appBean.setUsuarios(null);
	}

	public void trocaSenha() {
		EntityManager manager = this.getEntityManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		repository.trocaSenha(usuarioAnalisado.getId(),
				usuarioAnalisado.getSenha());
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Senha de " + usuarioAnalisado.getNome()
						+ " alterada com sucesso"));
	}

	public void autentica(ActionEvent actionEvent) throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		EntityManager manager = this.getEntityManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		Usuario usuarioTemp = repository.login(usuarioLogado.getLogin(),
				usuarioLogado.getSenha());
		if (usuarioTemp == null) {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Usu�rio inv�lido", "Usu�rio e/ou senha inv�lido"));
			this.usuarioLogado = null;
			this.loggedIn = false;
		} else {
			this.usuarioLogado = usuarioTemp;
			ExternalContext ec = fc.getExternalContext();
			HttpSession session = (HttpSession) ec.getSession(false);
			session.setAttribute("usuario", usuarioTemp);
			ServletContext servletContext = (ServletContext) ec.getContext();
			String webContentRoot = servletContext.getContextPath();
			ec.redirect(webContentRoot + "/index.xhtml");
			this.loggedIn = true;
		}
	}

	public void registraSaida() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpSession session = (HttpSession) ec.getSession(false);
		session.removeAttribute("usuario");
		ServletContext servletContext = (ServletContext) ec.getContext();
		String webContentRoot = servletContext.getContextPath();
		fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Voc� foi deslogado", "Usu�rio " + usuarioLogado.getNome()
						+ " deslogado"));
		ec.getFlash().setKeepMessages(true);
		ec.redirect(webContentRoot + "/login.xhtml");
		this.loggedIn = false;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuario) {
		this.usuarioLogado = usuario;
	}

	public Usuario getUsuarioAnalisado() {
		return usuarioAnalisado;
	}

	public void setUsuarioAnalisado(Usuario usuarioAnalisado) {
		this.usuarioAnalisado = usuarioAnalisado;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public AppBean getAppBean() {
		return appBean;
	}

	public void setAppBean(AppBean appBean) {
		this.appBean = appBean;
	}

}
