package br.com.viwax.iviwax.events;

import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import br.com.viwax.iviwax.model.Usuario;

public class AuthPhaseListener implements PhaseListener {

	@Override
	public void afterPhase(PhaseEvent event) {
		FacesContext fc = event.getFacesContext();
		ExternalContext ec = fc.getExternalContext();
		String currentPage = fc.getViewRoot().getViewId();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(
				true);
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		if (usuario == null) {
			if (!currentPage.equals("/login.xhtml")) {
				try {
					ServletContext servletContext = (ServletContext) ec
							.getContext();
					String webContentRoot = servletContext.getContextPath();
					System.out.println("Redirecionando... para Login");
					ec.redirect(webContentRoot + "/login.xhtml");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void beforePhase(PhaseEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}

}
