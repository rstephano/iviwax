package br.com.viwax.iviwax.installer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.viwax.iviwax.model.Usuario;

public class CriaUserMaster {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("iVIWAX-PU");
		EntityManager manager = factory.createEntityManager();

		Usuario usuario = new Usuario();
		usuario.setNome("Administrador");
		usuario.setAdmin(true);
		usuario.setLogin("admin");
		usuario.setSenha("admin");
		usuario.setEmail("contato@viwax.com.br");

		manager.persist(usuario);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

		manager.close();
		factory.close();
	}
}
