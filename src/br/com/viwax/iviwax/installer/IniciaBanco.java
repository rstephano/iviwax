package br.com.viwax.iviwax.installer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class IniciaBanco {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("iVIWAX-PU");
		EntityManager manager = factory.createEntityManager();
		factory.close();
	}
}
