package br.com.viwax.iviwax.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Contratos")
public class Contrato {
	@Id
	@GeneratedValue
	private Long id;
	@Column(nullable = false)
	private Date dataCriacao;
	@OneToOne
	@JoinColumn(name = "idProcesso")
	private Processo processo;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuarioCriador;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "idContrato")
	private List<Pagamento> eventosDePagamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Usuario getUsuarioCriador() {
		return usuarioCriador;
	}

	public void setUsuarioCriador(Usuario usuarioCriador) {
		this.usuarioCriador = usuarioCriador;
	}

	public List<Pagamento> getEventosDePagamento() {
		return eventosDePagamento;
	}

	public void setEventosDePagamento(List<Pagamento> eventosDePagamento) {
		this.eventosDePagamento = eventosDePagamento;
	}

}
