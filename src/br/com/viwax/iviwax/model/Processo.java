package br.com.viwax.iviwax.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Processos")
public class Processo {
	@Id
	@GeneratedValue
	private Long id;
	@Column(nullable = false)
	private int proposta;
	@Column(nullable = false)
	private int revisao;
	@OneToOne
	@JoinColumn(name = "idContrato")
	private Contrato contrato;
	@OneToOne
	@JoinColumn(name = "idEndereco")
	private Endereco endereco;

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getProposta() {
		return proposta;
	}

	public void setProposta(int proposta) {
		this.proposta = proposta;
	}

	public int getRevisao() {
		return revisao;
	}

	public void setRevisao(int revisao) {
		this.revisao = revisao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
