package br.com.viwax.iviwax.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.viwax.iviwax.model.Cliente;
import br.com.viwax.iviwax.model.Endereco;

public class ClienteRepository {
	private EntityManager manager;

	public ClienteRepository(EntityManager manager) {
		this.manager = manager;
	}

	public void adiciona(Cliente cliente) {
		this.manager.persist(cliente);
	}

	public Cliente buscar(Long id) {
		return manager.find(Cliente.class, id);
	}

	public void remover(Long id) {
		manager.remove(buscar(id));
	}

	public void atualizar(Cliente cliente, Long id) {
		Cliente clienteTemp = buscar(id);
		clienteTemp.setNome(cliente.getNome());
		clienteTemp.setRazaoSocial(cliente.getRazaoSocial());
		clienteTemp.setIm(cliente.getIm());
		clienteTemp.setIe(cliente.getIe());
		clienteTemp.setEnderecos(cliente.getEnderecos());
		clienteTemp.setContatos(cliente.getContatos());
	}

	public List<Cliente> buscaTodos() {
		Query query = this.manager.createQuery("select c from Cliente c");
		return query.getResultList();
	}

	public void adicionaEndereco(Endereco enderecoNovo) {
		this.manager.persist(enderecoNovo);
	}
}
