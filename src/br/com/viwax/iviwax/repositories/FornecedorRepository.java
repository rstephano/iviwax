package br.com.viwax.iviwax.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.viwax.iviwax.model.Fornecedor;

public class FornecedorRepository {
	private EntityManager manager;

	// CONSTRUTOR COM ENTITYMANAGER
	public FornecedorRepository(EntityManager manager) {
		this.manager = manager;
	}

	// FIND

	// CREATE
	public void adiciona(Fornecedor fornecedor) {
		this.manager.persist(fornecedor);
	}

	public Fornecedor buscar(Long id) {
		return manager.find(Fornecedor.class, id);
	}

	// BUSCA TODOS
	public List<Fornecedor> buscaTodos() {
		Query query = this.manager.createQuery("select f from Fornecedor f");
		return query.getResultList();
	}
}
