package br.com.viwax.iviwax.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.viwax.iviwax.model.Servico;

public class ServicoRepository {
	private EntityManager manager;

	public ServicoRepository(EntityManager manager) {
		this.manager = manager;
	}

	public void adiciona(Servico servico) {
		this.manager.persist(servico);
	}

	public Servico buscar(Long id) {
		return manager.find(Servico.class, id);
	}

	public List<Servico> buscaTodos() {
		Query query = this.manager.createQuery("select s from Servico s");
		return query.getResultList();
	}
}
