package br.com.viwax.iviwax.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.viwax.iviwax.model.Usuario;

/**
 * Documenta��o da classe br.com.viwax.iviwax.repositories.UsuarioRepository
 * 
 * @author Rafael Stephano
 * @version 1.0
 * 
 */
public class UsuarioRepository {
	/**
	 * EntityManager que ser� utilizado para realizar as transa��es
	 */
	private EntityManager manager;

	/**
	 * Construtor da classe
	 * 
	 * @author Rafael Stephano
	 * @param manager
	 *            EntityManager que ser� utilizado para realizar as transa��es
	 */
	public UsuarioRepository(EntityManager manager) {
		this.manager = manager;
	}

	/**
	 * Adiciona usu�rio no banco de dados via persist do Hibernate
	 * 
	 * @author Rafael Stephano
	 * @param usuario
	 */
	public void adiciona(Usuario usuario) {
		this.manager.persist(usuario);
	}

	/**
	 * Busca usu�rio no banco via find do Hibernate
	 * 
	 * @author Rafael Stephano
	 * @param id
	 * @return Usu�rio completo no banco de dados
	 */
	public Usuario buscar(Long id) {
		return manager.find(Usuario.class, id);
	}

	/**
	 * Remove usu�rio do banco de dados via remove do Hibernate
	 * 
	 * @author Rafael Stephano
	 * @param id
	 */
	public void remover(Long id) {
		manager.remove(buscar(id));
	}

	/**
	 * Atualiza os dados do usu�rio via setters e Hibernate
	 * 
	 * @author Rafael Stephano
	 * @param usuario
	 */
	public void atualizar(Usuario usuario, Long id) {
		Usuario usuarioTemp = buscar(id);
		usuarioTemp.setNome(usuario.getNome());
		usuarioTemp.setAdmin(usuario.isAdmin());
	}

	/**
	 * Lista todos os usu�rios registrados no banco de dados
	 * 
	 * @author Rafael Stephano
	 * @return Lista de usu�rios registrados no banco de dados
	 */
	public List<Usuario> buscaTodos() {
		Query query = this.manager.createQuery("select u from Usuario u");
		return query.getResultList();
	}

	/**
	 * Efetua login no sistema e salva usu�rio na sess�o
	 * 
	 * @author Rafael Stephano
	 * @param login
	 * @param senha
	 * @return Usu�rio registrado no sistema
	 */
	public Usuario login(String login, String senha) {
		Query query = this.manager.createQuery("from Usuario where login = '"
				+ login + "' and senha = '" + senha + "'");
		List<Usuario> usuarios = query.getResultList();
		if (usuarios.isEmpty()) {
			return null;
		} else {
			return usuarios.get(0);
		}
	}

	public void trocaSenha(Long id, String senha) {
		Usuario usuario = buscar(id);
		usuario.setSenha(senha);
	}
}
