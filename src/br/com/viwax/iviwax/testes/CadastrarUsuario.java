package br.com.viwax.iviwax.testes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.viwax.iviwax.model.Usuario;

public class CadastrarUsuario {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("iVIWAX-PU");
		EntityManager manager = factory.createEntityManager();

		Usuario usuario = new Usuario();
		usuario.setNome("Rafael Stephano");
		usuario.setLogin("rrstephano");
		usuario.setSenha("Senha2");
		manager.persist(usuario);
		manager.getTransaction().begin();
		manager.getTransaction().commit();
	}
}
